Gluster Admin Scripts
---------------------

A set of scripts used for general gluster admin tasks.

All scripts use a local file 'gl_nodelist', listing the hostnames of the nodes to be affected by the script.

* gluster_dmesg_xfs_check.sh - checks all nodes' dmesg output for XFS related issues
* gluster_file_brick_check.sh - interrogates the state of an unhealed file or directory, running ls/stat/getfattr on all instnaces of the file on all bricks
* gluster_fstrim_bricks.sh - runs fstrim on all bricks in the cluster. This brings the amount of allocated space in line with the used space
* gluster_gather_sosreports.sh - runs sosreport on all nodes, collecting the output in local directories
* gluster_restart_ctdb_service.sh - brings up the CTDB and supporting services in a working state, tackling several gotchas along the way
* gluster_switch_monitoring.sh - Swaps between Nagios/RHSC and Web Admin/Grafana monitoring systems
