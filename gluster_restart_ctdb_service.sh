#! /bin/sh
#
# Restart CTDB by stopping all instances, clearing bumf and starting
#
# Copyright (C) 2019  Mark Einon <mark.einon@gmail.com>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License,
# Version 2.1 only as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#

NODEFILE=gl_nodelist

usage()
{
cat << EOF
Usage: `basename $0` -u ads_username -p ads_password
Restarts cleanly the CTDB and Samba services on a gluster cluster

Mandatory arguments to long options are mandatory for short options too.
  -n, --nodelist=FILE   nodelist file, alternate to default ~/$NODEFILE
                        or \$PWD/$NODEFILE
  -p, --password=PASS   password to authorise joining AD domain
  -u, --username=USER	username to authorise joining AD domain

  -h, --help            display this help and exit
EOF
}

if [ -f ~/$NODEFILE ]; then
    NODES=`cat ~/$NODEFILE`
elif [ -f $NODEFILE ]; then
    NODES=`cat $NODEFILE`
fi

PARSEOPTS=`getopt -o hu:p:n: --long help,username:,password:,nodelist: \
           -n $0 -- "$@"`

while true; do
    case $1 in
        -h | --help )
            usage;
            exit 1
            ;;
        -u | --username )
            UN=$2
            shift 2
            ;;
        -p | --password )
            PW=$2
            shift 2
            ;;
        -n | --nodelist )
            NODES=`cat $2`
            shift 2
            ;;
        -- ) shift; break ;;
        * ) break ;;
    esac
done

if [[ ! -v PW || ! -v UN ]]; then
    echo "AD domain password and username are mandatory."
    echo
	usage;
	exit 1
fi

if [ ! -v NODES ];then
    echo "No nodelist found in ~/$NODEFILE, \$PWD/$NODEFILE or in nodelist parameter."
    echo
    usage;
    exit 1
fi

# First, stop all CTDB and Samba instances running on CTDB nodes
for NODE in ${NODES[@]}
do
    echo running on $NODE :
    ssh root@$NODE \
        'systemctl stop ctdb nmb smb winbind'
done

DATE=`date -Iseconds`

# Commands to restart CTDB cleanly on all nodes
COMMANDS=$(cat << EOF
mv /var/log/log.ctdb /var/log/log.ctdb${DATE};
echo 950000 > /sys/fs/cgroup/cpu,cpuacct/cpu.rt_runtime_us;
sysctl -w kernel.sched_rt_runtime_us=-1;
mount -a;
find /var/lib/samba -name "*.ldb" | xargs rm;
find /var/lib/samba -name "*.tdb" | xargs rm;
find /var/lib/samba -name "*.dat" | xargs rm;
find /var/lib/samba/lock -name "*.tdb" | xargs rm;
rm /var/lib/samba/smb_krb5/*;
rm /var/lib/samba/msg.lock/*;
rm /var/lib/samba/lock/msg.lock/*;
rm /var/lib/samba/private/msg.sock/*;
systemctl start ctdb;
systemctl start winbind;
net ads join -U ${UN}%${PW};
systemctl start nmb;
systemctl start smb;
ctdb status;
sleep 30s;
systemctl start nmb;
EOF
)

# Run the commands on all nodes one by one
for NODE in ${NODES[@]}
do
    ssh root@$NODE ${COMMANDS}
done

# Now compress the old CTDB logs
# This is done after to not delay the start of the CTDB services
for NODE in ${NODES[@]}
do
    ssh root@$NODE gzip /var/log/log.ctdb${DATE}
done
