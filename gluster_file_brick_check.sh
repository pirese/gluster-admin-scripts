#! /bin/sh
#
# Check all bricks on nodes in a gluster peer group for file / directory
# information for debugging purposes. Usually used to investigate
# gluster heal issues.
#
# Copyright (C) 2019  Mark Einon <mark.einon@gmail.com>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License,
# Version 2.1 only as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#

NODEFILE=gl_nodelist

usage()
{
cat << EOF
Usage: `basename $0` [OPTIONS]... -p GL_BRICK_PATH
Check all bricks on nodes in a gluster peer group for a file or dir, running
ls, stat and getfattr on them. Usually used to investigate gluster heal issues.

Mandatory arguments to long options are mandatory for short options too.
  -n, --nodelist=FILE   nodelist file, alternate to default ~/$NODEFILE
                        or \$PWD/$NODEFILE
  -p, --gl-path         gluster volume brick path to file or dir of interest

  -h, --help            display this help and exit
EOF
}

if [ -f ~/$NODEFILE ]; then
    NODES=`cat ~/$NODEFILE`
elif [ -f $NODEFILE ]; then
    NODES=`cat $NODEFILE`
fi

PARSEOPTS=`getopt -o hp:n: --long help,gl-path:,nodelist: \
           -n $0 -- "$@"`

while true; do
    case $1 in
        -h | --help )
            usage;
            exit 1
            ;;
        -p | --gl-path )
            LOC=$2
            shift 2
            ;;
        -n | --nodelist )
            NODES=`cat $2`
            shift 2
            ;;
        -- ) shift; break ;;
        * ) break ;;
    esac
done

if [ ! -v NODES ]; then
    echo "No nodelist found in ~/$NODEFILE, \$PWD/$NODEFILE or in nodelist parameter."
    echo
    usage;
    exit 1
fi

if [ ! -v LOC ]; then
    echo "No gluster volume brick path given."
    echo
    usage;
    exit 1
fi

COMMANDS=$(cat << EOF
for i in {1..80}; do
    if [ -e "/gluster/brick\$i/\$i/${LOC}" ]; then
        echo "--->Found on brick \$i";
        echo "ls -l output:";
        ls -l "/gluster/brick\$i/\$i/${LOC}";
        echo "stat output:";
        stat "/gluster/brick\$i/\$i/${LOC}";
        echo "getfattr output:";
        getfattr -e hex -m . -d  "/gluster/brick\$i/\$i/${LOC}" 2> /dev/null;
    fi
done
EOF
)

#echo ${COMMANDS}

for NODE in ${NODES[@]}
do
    echo "******* Checking $NODE ********":
    ssh root@$NODE ${COMMANDS}
done

