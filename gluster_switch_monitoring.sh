#! /bin/sh
#
# Switch between tendrl and nagios on all gluster nodes
#
# Copyright (C) 2019  Ellis Pires <pirese@cardiff.ac.uk>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License,
# Version 2.1 only as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#

NODEFILE=gl_nodelist

usage()
{
cat << EOF
Usage: `basename $0` [OPTIONS]...
Starts and enables tendrl services whilst stopping and disabling nagios
and storage console services or vice versa.

Mandatory arguments to long options are mandatory for short options too.
  -m, --monitoring=MONITORING	monitoring type; 'tendrl' or 'nagios'
  -n, --nodelist=FILE   nodelist file, alternate to default ~/$NODEFILE
                        or \$PWD/$NODEFILE

  -h, --help            display this help and exit
EOF
}

if [ -f ~/$NODEFILE ]; then
    NODES=`cat ~/$NODEFILE`
elif [ -f $NODEFILE ]; then
    NODES=`cat $NODEFILE`
fi

PARSEOPTS=`getopt -o hm:n: --long help,monitoring,nodelist: \
           -n $0 -- "$@"`

while true; do
    case $1 in
        -h | --help )
            usage;
            exit 1
            ;;
        -m | --monitoring )
            MONITORING=$2
            shift 2
            ;;
        -n | --nodelist )
            NODES=`cat $2`
            shift 2
            ;;
        -- ) shift; break ;;
        * ) break ;;
    esac
done

if [ ! -v NODES ];then
    echo "No nodelist found in ~/$NODEFILE, \$PWD/$NODEFILE or in nodelist parameter."
    echo
    usage;
    exit 1
fi

if [ "$MONITORING" == "nagios" ]; then
	COMMANDS=$(cat <<-EOF 
	systemctl stop tendrl-node-agent;
	systemctl disable tendrl-node-agent;
	systemctl stop tendrl-gluster-integration;
	systemctl disable tendrl-gluster-integration;
	systemctl stop collectd;
	systemctl disable collectd;
	systemctl start nrpe;
	systemctl enable nrpe;
	systemctl start vdsmd;
	systemctl enable vdsmd;
	EOF
	)
elif [ "$MONITORING" == "tendrl" ]; then
	COMMANDS=$(cat <<-EOF
	systemctl stop nrpe;
	systemctl disable nrpe;
	systemctl stop vdsmd;
	systemctl disable vdsmd; 
	systemctl start tendrl-node-agent;
	systemctl enable tendrl-node-agent;
	systemctl start tendrl-gluster-integration;
	systemctl enable tendrl-gluster-integration;
	systemctl start collectd;
	systemctl enable collectd;
	EOF
	)
else
	echo "Monitoring type must be either 'nagios' or 'tendrl'"
	echo
	usage;
	exit 1
fi

for NODE in ${NODES[@]}
do
    echo "running on $NODE: $COMMANDS"
    ssh root@$NODE ${COMMANDS}
done

