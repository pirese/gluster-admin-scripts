#! /bin/sh
#
# This script estimates the minimum arbiter brick size required
# for a given data brick, based on Red Hat's formula:
# 4KB * (data brick size / average file size)
# An estimate is only generated for bricks which belong to a
# volume that has quotas enabled.
#
# Copyright (C) 2020 Ellis Pires <pirese@cardiff.ac.uk>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License,
# Version 2.1 only as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#

NODEFILE=gl_nodelist

usage()
{
cat << EOF
Usage: `basename $0`
This program restarts the Red Hat Web Administration monitoring system by
shutting down all associated processes and restarting them in the correct
order. The program requires the list of gluster data nodes (gl_nodelist)
as well as a separate list containing the arbiter node (gl_arbiterlist).

Mandatory arguments to long options are mandatory for short options too.
  -n, --nodelist=FILE   nodelist file, alternate to default ~/$NODEFILE
                        or \$PWD/$NODEFILE
  -h, --help            display this help and exit
EOF
}

if [ -f ~/$NODEFILE ]; then
    NODES=`cat ~/$NODEFILE`
elif [ -f $NODEFILE ]; then
    NODES=`cat $NODEFILE`
fi

PARSEOPTS=`getopt -o hn:a: --long help,nodelist: \
           -n $0 -- "$@"`

while true; do
    case $1 in
        -h | --help )
            usage;
            exit 1
            ;;
        -n | --nodelist )
            NODES=`cat $2`
            shift 2
            ;;
        -- ) shift; break ;;
        * ) break ;;
    esac
done

if [ ! -v NODES ];then
    echo "No nodelist found in ~/$NODEFILE, \$PWD/$NODEFILE or in nodelist parameter."
    echo
    usage;
    exit 1
fi

clear

# Commands to generate minimum arbiter brick size estimates for each data brick on a node
COMMANDS=$(cat << EOF
NUM_BRICKS=\$(ls -f /gluster | grep -c brick);
for BRICK_NUM in \$(seq 1 \$NUM_BRICKS); do
    if [ -d "/gluster/brick\$BRICK_NUM/\$BRICK_NUM" ]; then
        FILE_INFO=\$(getfattr --absolute-names -d -m . -e hex /gluster/brick\$BRICK_NUM/\$BRICK_NUM \
                  | grep quota.size | cut -d"x" -f2 | sed -r 's/(.{16})/\1\n/g' \
                  | sed '/^$/d' | sed 's/^/0x/' | xargs printf '%d ');
        eval "FILE_INFO_ARRAY=(\$FILE_INFO)";
        if [ \${#FILE_INFO_ARRAY[@]} -eq 3 ]; then
            AVERAGE_FILE_SIZE=\$((FILE_INFO_ARRAY[0] / FILE_INFO_ARRAY[1]));
	    DATA_BRICK_SIZE=$((20*(1024**4)));
            MAX_FILES=\$((DATA_BRICK_SIZE / AVERAGE_FILE_SIZE));
	    BLOCK_SIZE=$((4*1024));
	    MIN_ARBITER_BRICK_SIZE=\$((\${MAX_FILES}*\${BLOCK_SIZE}));
            echo "Brick \$BRICK_NUM: \$((\${MIN_ARBITER_BRICK_SIZE} / 1024**3 )) GB";
        fi
    fi
done
EOF
)

# Execute commands for each node
echo Estimating minimum arbiter brick size for utilised data bricks on each node:
for NODE in ${NODES[@]}
do
    echo Estimations for $NODE:
    ssh root@$NODE ${COMMANDS}
done

