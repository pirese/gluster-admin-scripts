#! /bin/sh
#
# Run sosreport on all gluster nodes
#
# Copyright (C) 2019  Mark Einon <mark.einon@gmail.com>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License,
# Version 2.1 only as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#

NODEFILE=gl_nodelist

usage()
{
cat << EOF
Usage: `basename $0` [OPTIONS]...
Runs 'sosreport' and gathers all logs on a list of servers, then gathers
the reports locally.

Mandatory arguments to long options are mandatory for short options too.
  -c, --caseid=CASEID	RedHat case ID with which to tag the sosreport
  -n, --nodelist=FILE   nodelist file, alternate to default ~/$NODEFILE
                        or \$PWD/$NODEFILE

  -h, --help            display this help and exit
EOF
}

CID=0

if [ -f ~/$NODEFILE ]; then
    NODES=`cat ~/$NODEFILE`
elif [ -f $NODEFILE ]; then
    NODES=`cat $NODEFILE`
fi

PARSEOPTS=`getopt -o hc:n: --long help,caseid,nodelist: \
           -n $0 -- "$@"`

while true; do
    case $1 in
        -h | --help )
            usage;
            exit 1
            ;;
        -c | --caseid )
            CID=$2
            shift 2
            ;;
        -n | --nodelist )
            NODES=`cat $2`
            shift 2
            ;;
        -- ) shift; break ;;
        * ) break ;;
    esac
done

if [ ! -v NODES ];then
    echo "No nodelist found in ~/$NODEFILE, \$PWD/$NODEFILE or in nodelist parameter."
    echo
    usage;
    exit 1
fi

COMMANDS=$(cat << EOF
sosreport --all-logs --tmp-dir=/sosreport --case-id ${CID}
EOF
)

for NODE in ${NODES[@]}
do
    CPDIR="sosreport_${NODE}"
    echo "running on $NODE :"
    ssh root@$NODE ${COMMANDS}
    mkdir ${CPDIR}
    scp root@$NODE:/sosreport/* ${CPDIR}
done

